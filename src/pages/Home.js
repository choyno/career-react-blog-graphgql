import React from 'react';

import Carousel from "../components/Carousel"
import Posts from "../components/Posts";

const Home = () => {

  const carouselPosts = {
    posts: [
      {
        id: 1,
        slug: "how-to-1",
        imageUrl: "https://images.unsplash.com/photo-1588257076537-ecbdf55b55e8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2555&q=80",
        title: "How to write highly readable React code — 10 coding style tips.",
        createdAt: "2020.07.03",
        comments: [ { comment: "Asds"} ]
      },

      {
        id: 2,
        slug: "how-to-2",
        imageUrl: "https://images.unsplash.com/photo-1586944672860-2a606666a887?ixlib=rb-1.2.1&auto=format&fit=crop&w=755&q=80",
        title: "Your so high readable Smoke code",
        createdAt: "2020.07.04",
        comments: []
      },
    ]
  };

  return (
    <div className="top">
        <Carousel carouselPosts={carouselPosts} />
        <Posts postItems={carouselPosts.posts} />
    </div>
  );
}
export default Home;
