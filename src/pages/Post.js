import React, {} from "react";

import { Link } from "react-router-dom";

import Comment from "../components/Comment";
import Breadcrumbs from "../components/common/Breadcrumbs";

import { postDate } from "../utils/helpers.js";

const Post = () => {

  const post = {
    title: "Title herer",
    slug: "slug-here",
    imageUrl: "https://images.unsplash.com/photo-1586944672860-2a606666a887?ixlib=rb-1.2.1&auto=format&fit=crop&w=755&q=80",
    createdAt: "2020.08.30",
    body: "From casting the perfect actors to portray crime-fighters and the troublemakers they aim to bring down, to locking in the perfect script that honors the comic book source material and delivers something fresh to audiences of all ages, to building the in-universe world with custom sets and high-quality CGI, the pro"
  }

  return (
    <div className="post">
      <div>
        <Breadcrumbs
          currentPage={post.title}
          currentPageUrl={"/posts/" + post.slug}
        />
        <div className="u-container">
          <div className="post-header">
            <ul className="post-action-list">
              <li className="post-action-item">
                <Link to={"/post/slug-here/edit"} className="post-action-link">
                    Edit Post
                </Link>
              </li>
            </ul>
          </div>
          <div className="post-body">
            <time
              dateTime={postDate(post.createdAt)}
              className="post-created"
            >
              {postDate(post.createdAt)}
            </time>
            <h1 className="post-title">{post.title}</h1>
            <div
              className="post-image"
              style={{ backgroundImage: `url(${post.imageUrl})` }}
            />
            <div className="post-text">{post.body}</div>
          </div>

          <Comment
            postId={"slug-here"}
            userId={"1232"}
            comments={"asd"}
          />

        </div>
      </div>
    </div>
  );
};

export default Post;
