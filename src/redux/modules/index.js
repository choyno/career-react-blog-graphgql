import { combineReducers } from 'redux'
// reducerImports   (DONT DELETE THIS LINE: USED FOR BATTLECRY DUCK GENERATOR)

import utils from "./utils/utilsReducer";

const reducer = combineReducers({
  utils
});

export default reducer

